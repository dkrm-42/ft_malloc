#include "malloc.h"

int	main(void)
{
	char	*fix;

	fix = malloc(1024);
	fix = malloc(1024 * 32);
	fix = malloc(1024 * 1024);
	fix = malloc(1024 * 1024 * 16);
	fix = malloc(1024 * 1024 * 128);
	show_alloc_mem();
	return (0);
}
