#!/usr/bin/env fish

for test_file in test*.c
	set_color -o blue; echo -n '#### > Test: '$test_file '< ####'; set_color normal; echo
	./run.sh $test_file
	set_color -o magenta; echo -n '==================================='; set_color normal; echo
end
