#!/bin/sh
cd $HOME/dev/ft_malloc/test/scale
clang -Wall -Wextra -Werror -L ../.. -lft_malloc -I../../src $@
export DYLD_LIBRARY_PATH=../..
export DYLD_INSERT_LIBRARIES="../../libft_malloc.so"
export DYLD_FORCE_FLAT_NAMESPACE=1 
command time -l ./a.out
rm -f a.out
