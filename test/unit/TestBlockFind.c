#include "unity.h"
#include "stdlib.h"
#include "ft_malloc.h"
#include <string.h>

#define NB_BLOCKS 5

static t_mem_pool	*pool;
static t_mem_block	*blocks[NB_BLOCKS];
static t_mem_block	*list;
static t_mem_block	*new_head;
static t_mem_block	*new_block;

void setUp(void)
{
	pool = pool_alloc_new_pool(4096);
	for (size_t i = 0; i < NB_BLOCKS; ++i)
		blocks[i] = pool_get_free_block(&pool);

	blocks[0]->addr = (void*)0x4000;
	blocks[0]->size = 0x10;
	blocks[0]->next = blocks[1];

	blocks[1]->addr = (void*)0xa000;
	blocks[1]->size = 0x200;
	blocks[1]->next = blocks[2];

	blocks[2]->addr = (void*)0x10000;
	blocks[2]->size = 0x400;
	blocks[2]->next = blocks[3];

	blocks[3]->addr = (void*)0xf0000;
	blocks[3]->size = 0x10;
	blocks[3]->next = blocks[4];

	blocks[4]->addr = (void*)0xf0200;
	blocks[4]->size = 0x100;
	blocks[4]->next = NULL;

	list = blocks[0];
	new_head = NULL;
}

void tearDown(void)
{
	internal_munmap_wrapper(pool, 4096);
}

// Ne pas avoir de block de taille suffisante
void test_FindFittestForSize_NoMatchShouldReturnNULL(void)
{
	new_block = block_find_fittest_for_size(&list, &pool, 0x410);

	// new list head
	TEST_ASSERT_EQUAL_PTR(blocks[0], list);

	TEST_ASSERT_NULL(                new_block);
	TEST_ASSERT_EQUAL(            5, pool->used); // Rien n'a ete rendu dans le pool
	TEST_ASSERT_NULL(                pool->freelist);

	// Assert the remaining list
	TEST_ASSERT_EQUAL_PTR(   0x4000, list->addr);
	TEST_ASSERT_EQUAL(         0x10, list->size);
	TEST_ASSERT_EQUAL_PTR(blocks[1], list->next);

	TEST_ASSERT_EQUAL_PTR(   0xa000, list->next->addr);
	TEST_ASSERT_EQUAL(        0x200, list->next->size);
	TEST_ASSERT_EQUAL_PTR(blocks[2], list->next->next);

	TEST_ASSERT_EQUAL_PTR(  0x10000, list->next->next->addr);
	TEST_ASSERT_EQUAL(        0x400, list->next->next->size);
	TEST_ASSERT_EQUAL_PTR(blocks[3], list->next->next->next);

	TEST_ASSERT_EQUAL_PTR(  0xf0000, list->next->next->next->addr);
	TEST_ASSERT_EQUAL(         0x10, list->next->next->next->size);
	TEST_ASSERT_EQUAL_PTR(blocks[4], list->next->next->next->next);

	TEST_ASSERT_EQUAL_PTR(  0xf0200, list->next->next->next->next->addr);
	TEST_ASSERT_EQUAL(        0x100, list->next->next->next->next->size);
	TEST_ASSERT_NULL(                list->next->next->next->next->next);
}

// Retourne bien le best fit, et non first fit
void test_FindFittestForSize_FindPerfectMatchAndNotFirstMatch(void)
{
	new_block = block_find_fittest_for_size(&list, &pool, 0x100);

	// new list head
	TEST_ASSERT_EQUAL_PTR(blocks[0], list);

	TEST_ASSERT_EQUAL_PTR(blocks[4], new_block);
	TEST_ASSERT_EQUAL(            5, pool->used); // Rien n'a ete rendu dans le pool
	TEST_ASSERT_NULL(                pool->freelist);

	TEST_ASSERT_EQUAL_PTR(  0xf0200, new_block->addr);
	TEST_ASSERT_EQUAL(        0x100, new_block->size);
	TEST_ASSERT_NULL(                new_block->next);

	// Assert the remaining list
	TEST_ASSERT_EQUAL_PTR(   0x4000, list->addr);
	TEST_ASSERT_EQUAL(         0x10, list->size);
	TEST_ASSERT_EQUAL_PTR(blocks[1], list->next);

	TEST_ASSERT_EQUAL_PTR(   0xa000, list->next->addr);
	TEST_ASSERT_EQUAL(        0x200, list->next->size);
	TEST_ASSERT_EQUAL_PTR(blocks[2], list->next->next);

	TEST_ASSERT_EQUAL_PTR(  0x10000, list->next->next->addr);
	TEST_ASSERT_EQUAL(        0x400, list->next->next->size);
	TEST_ASSERT_EQUAL_PTR(blocks[3], list->next->next->next);

	TEST_ASSERT_EQUAL_PTR(  0xf0000, list->next->next->next->addr);
	TEST_ASSERT_EQUAL(         0x10, list->next->next->next->size);
	TEST_ASSERT_NULL(                list->next->next->next->next);
}

void test_FindFittestForSize_TwoBlocksMatchesReturnTheFirstOne(void)
{
	new_block = block_find_fittest_for_size(&list, &pool, 0x10);

	// new list head
	TEST_ASSERT_EQUAL_PTR(blocks[1], list);

	TEST_ASSERT_EQUAL_PTR(blocks[0], new_block);
	TEST_ASSERT_EQUAL(            5, pool->used); // Rien n'a ete rendu dans le pool
	TEST_ASSERT_NULL(                pool->freelist);

	TEST_ASSERT_EQUAL_PTR(   0x4000, new_block->addr);
	TEST_ASSERT_EQUAL(         0x10, new_block->size);
	TEST_ASSERT_NULL(                new_block->next);

	// Assert the remaining list
	TEST_ASSERT_EQUAL_PTR(   0xa000, list->addr);
	TEST_ASSERT_EQUAL(        0x200, list->size);
	TEST_ASSERT_EQUAL_PTR(blocks[2], list->next);
	TEST_ASSERT_EQUAL_PTR(  0x10000, list->next->addr);
	TEST_ASSERT_EQUAL(        0x400, list->next->size);
	TEST_ASSERT_EQUAL_PTR(blocks[3], list->next->next);
	TEST_ASSERT_EQUAL_PTR(  0xf0000, list->next->next->addr);
	TEST_ASSERT_EQUAL(         0x10, list->next->next->size);
	TEST_ASSERT_EQUAL_PTR(blocks[4], list->next->next->next);
	TEST_ASSERT_EQUAL_PTR(  0xf0200, list->next->next->next->addr);
	TEST_ASSERT_EQUAL(        0x100, list->next->next->next->size);
	TEST_ASSERT_NULL(                list->next->next->next->next);
}
