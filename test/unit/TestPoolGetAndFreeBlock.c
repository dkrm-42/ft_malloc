#include "unity.h"
#include "ft_malloc.h"
#include <string.h>
#include <stdio.h>
#include <sys/mman.h>

#define MAX_BLOCKS_IN_POOL			((POOL_SIZE - sizeof(t_mem_pool)) / sizeof(t_mem_block))

static t_mem_pool	*p;
static t_mem_block	*b;

void setUp(void)
{
	p = NULL;
}

void tearDown(void)
{
	munmap(p, POOL_SIZE);
}


void test_PoolGetFreeBlock_GetOneBlockOnly(void)
{
	b = pool_get_free_block(&p);

	TEST_ASSERT_NOT_NULL(b);
	TEST_ASSERT_EQUAL(1, p->offset);
	TEST_ASSERT_EQUAL(1, p->used);
	TEST_ASSERT_NULL(p->freelist);
	TEST_ASSERT_NULL(p->next);
	TEST_ASSERT_EQUAL(&p->blocks[0], b);

	memset(b, 0xFF, sizeof(*b));
}

void test_PoolGetFreeBlock_GetAllBlocksAndWriteToThem(void)
{
	for (size_t i = 0; i < MAX_BLOCKS_IN_POOL; ++i)
	{
		b = pool_get_free_block(&p);

		TEST_ASSERT_NOT_NULL(b);
		TEST_ASSERT_EQUAL(i + 1, p->offset);
		TEST_ASSERT_EQUAL(i + 1, p->used);
		TEST_ASSERT_NULL(p->freelist);
		TEST_ASSERT_NULL(p->next);
		TEST_ASSERT_EQUAL(&p->blocks[i], b);

		memset(b, 0xFF, sizeof(*b));
	}

	// Assert that sanitize will crash
	//memset(&p->blocks[MAX_BLOCKS_IN_POOL], 0xFF, sizeof(*b));
}

void test_PoolGetFreeBlock_GetAllBlocksInPoolAndAnotherOneToCheckThatItIsInAnotherPool(void)
{
	for (size_t i = 0; i < MAX_BLOCKS_IN_POOL; ++i)
	{
		b = pool_get_free_block(&p);

		TEST_ASSERT_NOT_NULL(b);
		TEST_ASSERT_EQUAL(i + 1, p->offset);
		TEST_ASSERT_EQUAL(i + 1, p->used);
		TEST_ASSERT_NULL(p->freelist);
		TEST_ASSERT_NULL(p->next);
		TEST_ASSERT_EQUAL(&p->blocks[i], b);

		memset(b, 0xFF, sizeof(*b));
	}

	TEST_ASSERT_EQUAL(MAX_BLOCKS_IN_POOL, p->offset);
	TEST_ASSERT_EQUAL(MAX_BLOCKS_IN_POOL, p->used);
	TEST_ASSERT_NULL(p->freelist);
	TEST_ASSERT_NULL(p->next);

	// Create new pool
	b = pool_get_free_block(&p);

	TEST_ASSERT_NOT_NULL(b);
	TEST_ASSERT_EQUAL(MAX_BLOCKS_IN_POOL, p->offset);
	TEST_ASSERT_EQUAL(MAX_BLOCKS_IN_POOL, p->used);
	TEST_ASSERT_NULL(p->freelist);
	TEST_ASSERT_NOT_NULL(p->next);

	t_mem_pool *pp = p->next;
	TEST_ASSERT_EQUAL(1, pp->offset);
	TEST_ASSERT_EQUAL(1, pp->used);
	TEST_ASSERT_NULL(pp->freelist);
	TEST_ASSERT_NULL(pp->next);

	// Check that the new block is not in the previous pool
	TEST_ASSERT((void*)b < (void*)p || (void*)b > (void*)((char*)p + p->allocated_size));
	TEST_ASSERT((void*)b > (void*)pp && (void*)b < (void*)((char*)pp + pp->allocated_size));
	TEST_ASSERT_EQUAL(&pp->blocks[0], b);

	memset(b, 0xFF, sizeof(*b));

	//printf("Pool addr: %p\n", (void*)p);
	//printf("Last Pool addr: %p\n", (void*)((char*)p + p->allocated_size));
	//printf("Last block addr: %p\n", (void*)b);
}

void test_PoolGetFreeBlock_Requiere100000BlocksAndCheckTheNumberOfPools(void)
{
	// Pour un pool de taille 4096, il y a 168 blocks de dispo

	size_t number_of_blocks_wanted = 100000;
	size_t blocks_allocated;

	for (size_t i = 0; i < number_of_blocks_wanted; ++i)
	{
		b = pool_get_free_block(&p);

		TEST_ASSERT_NOT_NULL(b);
		TEST_ASSERT_NULL(p->freelist);

		memset(b, 0xFF, sizeof(*b));
	}

	// Number of expected pools
	size_t expected_number_of_pools = number_of_blocks_wanted / MAX_BLOCKS_IN_POOL;
	if (number_of_blocks_wanted % MAX_BLOCKS_IN_POOL > 0 || number_of_blocks_wanted < MAX_BLOCKS_IN_POOL)
		expected_number_of_pools += 1;

	// How many pools do we have ?
	size_t number_of_pools;
	for (number_of_pools = 0; p->next != NULL; p = p->next)
	{
		TEST_ASSERT_EQUAL(MAX_BLOCKS_IN_POOL, p->offset);
		TEST_ASSERT_EQUAL(MAX_BLOCKS_IN_POOL, p->used);
		TEST_ASSERT_NULL(p->freelist);
		//TEST_ASSERT_NOT_NULL(p->next);
		number_of_pools += 1;
	}

	// Last pool may not be full
	number_of_pools += 1;
	TEST_ASSERT_EQUAL(number_of_blocks_wanted % MAX_BLOCKS_IN_POOL, p->offset);
	TEST_ASSERT_EQUAL(number_of_blocks_wanted % MAX_BLOCKS_IN_POOL, p->used);
	TEST_ASSERT_NULL(p->freelist);
	TEST_ASSERT_NULL(p->next);

	TEST_ASSERT_EQUAL(expected_number_of_pools, number_of_pools);
}

void test_PoolReleaseBlock_DemandOneReleaseOne(void)
{
	void	*first_block;

	// demand
	b = pool_get_free_block(&p);
	first_block = b;
	TEST_ASSERT_NOT_NULL(b);
	TEST_ASSERT_EQUAL(1, p->offset);
	TEST_ASSERT_EQUAL(1, p->used);
	TEST_ASSERT_NULL(p->freelist);
	TEST_ASSERT_NULL(p->next);
	TEST_ASSERT_EQUAL_PTR(&p->blocks[0], b);

	memset(b, 0xFF, sizeof(*b));

	// release
	p = pool_release_block(p, b);
	TEST_ASSERT_EQUAL(1, p->offset);
	TEST_ASSERT_EQUAL(0, p->used);
	TEST_ASSERT_NOT_NULL(p->freelist);
	TEST_ASSERT_EQUAL_PTR(b, p->freelist);
	TEST_ASSERT_NULL(p->freelist->next);
	TEST_ASSERT_NULL(p->next);

	// demand again
	b = pool_get_free_block(&p);
	TEST_ASSERT_NOT_NULL(b);
	TEST_ASSERT_EQUAL_PTR(first_block, b);
	TEST_ASSERT_EQUAL(1, p->offset);
	TEST_ASSERT_EQUAL(1, p->used);
	TEST_ASSERT_NULL(p->freelist);
	TEST_ASSERT_NULL(p->next);
	TEST_ASSERT_EQUAL_PTR(&p->blocks[0], b);

	memset(b, 0xFF, sizeof(*b));
}

void test_PoolReleaseBlock_ReleaseAddrThatDoesntBelongsToPool(void)
{
	t_mem_block *b[3];
	b[0] = pool_get_free_block(&p);
	b[1] = pool_get_free_block(&p);
	b[2] = pool_get_free_block(&p);
	p = pool_release_block(p, b[1]);

	TEST_ASSERT_EQUAL(3, p->offset);
	TEST_ASSERT_EQUAL(2, p->used);
	TEST_ASSERT_EQUAL_PTR(b[1], p->freelist);
	TEST_ASSERT_NULL(p->freelist->next);
	TEST_ASSERT_NULL(p->next);

	p = pool_release_block(p, (void*)0x123456);

	TEST_ASSERT_EQUAL(3, p->offset);
	TEST_ASSERT_EQUAL(2, p->used);
	TEST_ASSERT_EQUAL_PTR(b[1], p->freelist);
	TEST_ASSERT_NULL(p->freelist->next);
	TEST_ASSERT_NULL(p->next);

	p = pool_release_block(p, (void*)NULL);

	TEST_ASSERT_EQUAL(3, p->offset);
	TEST_ASSERT_EQUAL(2, p->used);
	TEST_ASSERT_EQUAL_PTR(b[1], p->freelist);
	TEST_ASSERT_NULL(p->freelist->next);
	TEST_ASSERT_NULL(p->next);
}

void test_PoolReleaseBlock_Acquire3BlocksReleaseTheMiddleOne(void)
{
	t_mem_block *b[3];
	b[0] = pool_get_free_block(&p);
	b[1] = pool_get_free_block(&p);
	b[2] = pool_get_free_block(&p);
	p = pool_release_block(p, b[1]);

	TEST_ASSERT_EQUAL(3, p->offset);
	TEST_ASSERT_EQUAL(2, p->used);
	TEST_ASSERT_EQUAL_PTR(b[1], p->freelist);
	TEST_ASSERT_NULL(p->freelist->next);
	TEST_ASSERT_NULL(p->next);

	t_mem_block *old_one = pool_get_free_block(&p);

	TEST_ASSERT_EQUAL(3, p->offset);
	TEST_ASSERT_EQUAL(3, p->used);
	TEST_ASSERT_EQUAL_PTR(b[1], old_one);
	TEST_ASSERT_NULL(p->freelist);
	TEST_ASSERT_NULL(p->next);

}

// Demander un pool complet, 1 de plus, rendre le premier du pool, puis le dernier du pool, puis le premier du second pool
void test_PoolReleaseBlock_GetOneFullPoolGetOneMoreBlockReleaseFirstBlockReleaseLastFirstPoolBlockThenLastBlock(void)
{
	t_mem_block *blocks[MAX_BLOCKS_IN_POOL];

	// first pool
	for (size_t i = 0; i < MAX_BLOCKS_IN_POOL; ++i)
	{
		blocks[i] = pool_get_free_block(&p);
	}
	TEST_ASSERT_EQUAL(p->max, p->offset);
	TEST_ASSERT_EQUAL(p->max, p->used);
	TEST_ASSERT_NULL(p->freelist);
	TEST_ASSERT_NULL(p->next);


	// second pool
	t_mem_block *first_second_pool = pool_get_free_block(&p);
	TEST_ASSERT_EQUAL(1, p->next->offset);
	TEST_ASSERT_EQUAL(1, p->next->used);
	TEST_ASSERT_NULL(p->next->freelist);
	TEST_ASSERT_NOT_NULL(p->next);


	// release first block of first pool
	p = pool_release_block(p, blocks[0]);
	TEST_ASSERT_EQUAL(p->max, p->offset);
	TEST_ASSERT_EQUAL(p->max - 1, p->used);
	TEST_ASSERT_NOT_NULL(p->freelist);
	TEST_ASSERT_EQUAL_PTR(blocks[0], p->freelist);

	TEST_ASSERT_EQUAL(1, p->next->offset);
	TEST_ASSERT_EQUAL(1, p->next->used);
	TEST_ASSERT_NULL(p->next->freelist);


	// release last block of first pool
	//printf("First Pool addr: %p\n", (void*)p);
	//printf("Last block addr: %p\n", (void*)blocks[p->max - 1]);
	//printf("Last Pool byte: %p\n", (void*)((char*)p + p->allocated_size));
	//printf("Second Pool addr: %p\n", (void*)p->next);
	p = pool_release_block(p, blocks[p->max - 1]);
	TEST_ASSERT_EQUAL(p->max, p->offset);
	TEST_ASSERT_EQUAL(p->max - 2, p->used);
	TEST_ASSERT_NOT_NULL(p->freelist);
	TEST_ASSERT_EQUAL_PTR(blocks[p->max - 1], p->freelist);
	TEST_ASSERT_EQUAL_PTR(blocks[0], p->freelist->next);

	TEST_ASSERT_EQUAL(1, p->next->offset);
	TEST_ASSERT_EQUAL(1, p->next->used);
	TEST_ASSERT_NULL(p->next->freelist);


	// release first block of second pool
	p = pool_release_block(p, first_second_pool);
	TEST_ASSERT_EQUAL(p->max, p->offset);
	TEST_ASSERT_EQUAL(p->max - 2, p->used);
	TEST_ASSERT_NOT_NULL(p->freelist);
	TEST_ASSERT_EQUAL_PTR(blocks[p->max - 1], p->freelist);
	TEST_ASSERT_EQUAL_PTR(blocks[0], p->freelist->next);

	TEST_ASSERT_EQUAL(1, p->next->offset);
	TEST_ASSERT_EQUAL(0, p->next->used);
	TEST_ASSERT_NOT_NULL(p->next->freelist);
	TEST_ASSERT_EQUAL_PTR(first_second_pool, p->next->freelist);
	TEST_ASSERT_NULL(p->next->freelist->next);
}

// Demander 3 pools complet. Rendre tout les blocks du second pool. Est-il unmap ?
void test_PoolReleaseBlock_Get3FullPoolsReleaseTheMiddleOneShouldUnmapIt(void)
{
	t_mem_block *blocks[3][MAX_BLOCKS_IN_POOL];

	// acquire 3 pools
	for (size_t i = 0; i < 3; ++i)
	{
		for (size_t j = 0; j < MAX_BLOCKS_IN_POOL; ++j)
		{
			blocks[i][j] = pool_get_free_block(&p);
		}
	}
	t_mem_pool *first = p;
	t_mem_pool *second = p->next;
	t_mem_pool *third = p->next->next;

	// release the middle one, second pool
	for (size_t i = 0, max = second->max; i < max; ++i)
		p = pool_release_block(p, blocks[1][i]);
	TEST_ASSERT_EQUAL_PTR(first, p);
	TEST_ASSERT_EQUAL_PTR(third, p->next);
	TEST_ASSERT_NULL(p->next->next);

	// acquire one block
	t_mem_block *b = pool_get_free_block(&p);
	TEST_ASSERT_EQUAL_PTR(first, p);
	TEST_ASSERT_EQUAL_PTR(third, p->next);
	t_mem_pool *new_third = p->next->next;
	TEST_ASSERT_NOT_NULL(p->next->next);
	TEST_ASSERT_NULL(p->next->next->next);
	TEST_ASSERT_EQUAL_PTR(&new_third->blocks[0], b);
}

// Demander 4 pools complet. Rendre tout les blocks du 3ème. Est-il unmap ? Rendre tout les blocks du premier. Est-il unmap ?
void test_PoolReleaseBlock_Get4FullPoolsReleaseTheThirdOneThenReleaseTheFirstOneShouldUnmapIt(void)
{
	t_mem_block *blocks[4][MAX_BLOCKS_IN_POOL];

	// acquire 4 pools
	for (size_t i = 0; i < 4; ++i)
	{
		for (size_t j = 0; j < MAX_BLOCKS_IN_POOL; ++j)
		{
			blocks[i][j] = pool_get_free_block(&p);
		}
	}
	t_mem_pool *first = p;
	t_mem_pool *second = p->next;
	t_mem_pool *third = p->next->next;
	t_mem_pool *fourth = p->next->next->next;

	// release third one. Shound unmap it
	for (size_t i = 0, max = third->max; i < max; ++i)
		p = pool_release_block(p, blocks[2][i]);
	TEST_ASSERT_EQUAL_PTR(first, p);
	TEST_ASSERT_EQUAL_PTR(second, p->next);
	TEST_ASSERT_EQUAL_PTR(fourth, p->next->next);
	TEST_ASSERT_NULL(p->next->next->next);

	// release first one. Should unmap it
	for (size_t i = 0, max = first->max; i < max; ++i)
		p = pool_release_block(p, blocks[0][i]);
	TEST_ASSERT_EQUAL_PTR(second, p);
	TEST_ASSERT_EQUAL_PTR(fourth, p->next);
	TEST_ASSERT_NULL(p->next->next);

	// acquire one block
	t_mem_block *b = pool_get_free_block(&p);
	TEST_ASSERT_EQUAL_PTR(second, p);
	TEST_ASSERT_EQUAL_PTR(fourth, p->next);
	t_mem_pool *new_third = p->next->next;
	TEST_ASSERT_NOT_NULL(p->next->next);
	TEST_ASSERT_NULL(p->next->next->next);
	TEST_ASSERT_EQUAL_PTR(&new_third->blocks[0], b);
}
