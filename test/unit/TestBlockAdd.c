#include "unity.h"
#include "stdlib.h"
#include "ft_malloc.h"
#include <string.h>

#define NB_BLOCKS 3

static t_mem_pool	*pool;
static t_mem_block	*blocks[NB_BLOCKS];
static t_mem_block	*list;
static t_mem_block	*new_head;
static t_mem_block	*new_block;

void setUp(void)
{
	pool = pool_alloc_new_pool(4096);

	new_block = pool_get_free_block(&pool);

	for (size_t i = 0; i < NB_BLOCKS; ++i)
		blocks[i] = pool_get_free_block(&pool);

	blocks[0]->addr = (void*)0x4000;
	blocks[0]->size = 0x10;
	blocks[0]->next = blocks[1];

	blocks[1]->addr = (void*)0xa000;
	blocks[1]->size = 0x200;
	blocks[1]->next = blocks[2];

	blocks[2]->addr = (void*)0x10000;
	blocks[2]->size = 0x400;
	blocks[2]->next = NULL;

	list = blocks[0];
	new_head = NULL;
}

void tearDown(void)
{
	internal_munmap_wrapper(pool, 4096);
}

void test_BlockSplitToSize_UseOneBigBlockAndResizeIt(void)
{
	t_mem_block *big_block = blocks[0];
	big_block->addr = (void*)0x01234;
	big_block->size = 40;
	big_block->next = (void*)0xdeadbeef;

	new_block = blocks[1];
	new_block->addr = (void*)0xeeee;
	new_block->size = 999;
	new_block->next = (void*)0xbaba;

	block_split_to_size(big_block, new_block, 12);

	TEST_ASSERT_EQUAL(40 - 12, big_block->size);
	TEST_ASSERT_EQUAL_PTR((char*)0x1234 + 12, big_block->addr);
	TEST_ASSERT_EQUAL_PTR(0xdeadbeef, big_block->next);
	TEST_ASSERT_EQUAL(12, new_block->size);
	TEST_ASSERT_EQUAL_PTR((char*)0x1234, new_block->addr);
	TEST_ASSERT_NULL(new_block->next);
}

// Ajouter un block en tete
void test_BlockAddToList_AddBlockInFrontWithNoMerge(void)
{
	new_block->addr = (void*)0x444;
	new_block->size = 32;
	new_block->next = NULL;

	new_head = block_add_to_list(list, new_block, &pool);

	TEST_ASSERT_EQUAL_PTR(new_block, new_head);

	TEST_ASSERT_EQUAL_PTR(    0x444, new_head->addr);
	TEST_ASSERT_EQUAL(           32, new_head->size);
	TEST_ASSERT_EQUAL_PTR(blocks[0], new_head->next);

	TEST_ASSERT_EQUAL_PTR(   0x4000, new_head->next->addr);
	TEST_ASSERT_EQUAL(           16, new_head->next->size);
	TEST_ASSERT_EQUAL_PTR(blocks[1], new_head->next->next);

	TEST_ASSERT_EQUAL_PTR(   0xa000, new_head->next->next->addr);
	TEST_ASSERT_EQUAL(          512, new_head->next->next->size);
	TEST_ASSERT_EQUAL_PTR(blocks[2], new_head->next->next->next);

	TEST_ASSERT_EQUAL_PTR(  0x10000, new_head->next->next->next->addr);
	TEST_ASSERT_EQUAL(         1024, new_head->next->next->next->size);
	TEST_ASSERT_NULL(                new_head->next->next->next->next);
}

// Ajouter un block en tete et merge avec le block suivant
void test_BlockAddToList_AddBlockInFrontAndMergeWithTheNextOne(void)
{
	new_block->size = 16;
	new_block->addr = (void*)0x3FF0; // (char*)0x4000 - new_block->size;
	new_block->next = NULL;

	new_head = block_add_to_list(list, new_block, &pool);

	TEST_ASSERT_EQUAL_PTR(new_block, new_head);

	TEST_ASSERT_EQUAL_PTR(   0x3FF0, new_head->addr);
	TEST_ASSERT_EQUAL(      16 + 16, new_head->size);
	TEST_ASSERT_EQUAL_PTR(blocks[1], new_head->next);

	TEST_ASSERT_EQUAL_PTR(   0xa000, new_head->next->addr);
	TEST_ASSERT_EQUAL(          512, new_head->next->size);
	TEST_ASSERT_EQUAL_PTR(blocks[2], new_head->next->next);

	TEST_ASSERT_EQUAL_PTR(  0x10000, new_head->next->next->addr);
	TEST_ASSERT_EQUAL(         1024, new_head->next->next->size);
	TEST_ASSERT_NULL(                new_head->next->next->next);
}

// Ajouter un block en second et merge avec le premier block et le block suivant
void test_BlockAddToList_AddBlockInFrontAndMergeWithFirstAndNextOne(void)
{
	new_block->size = 0x5ff0;
	new_block->addr = (void*)0x4010;
	new_block->next = NULL;

	new_head = block_add_to_list(list, new_block, &pool);

	TEST_ASSERT_EQUAL_PTR(blocks[0], new_head);

	TEST_ASSERT_EQUAL_PTR(   0x4000, new_head->addr);
	TEST_ASSERT_EQUAL(       0x6200, new_head->size);
	TEST_ASSERT_EQUAL_PTR(blocks[2], new_head->next);

	TEST_ASSERT_EQUAL_PTR(  0x10000, new_head->next->addr);
	TEST_ASSERT_EQUAL(        0x400, new_head->next->size);
	TEST_ASSERT_NULL(                new_head->next->next);
}

// Ajouter un block au milieu
void test_BlockAddToList_AddBlockInTheMiddleWithNoMerge(void)
{
	new_block->size = 16;
	new_block->addr = (void*)0x9FE0;
	new_block->next = NULL;

	new_head = block_add_to_list(list, new_block, &pool);

	TEST_ASSERT_EQUAL_PTR(blocks[0], new_head);

	TEST_ASSERT_EQUAL_PTR(   0x4000, new_head->addr);
	TEST_ASSERT_EQUAL(           16, new_head->size);
	TEST_ASSERT_EQUAL_PTR(new_block, new_head->next);

	TEST_ASSERT_EQUAL_PTR(   0x9FE0, new_head->next->addr);
	TEST_ASSERT_EQUAL(           16, new_head->next->size);
	TEST_ASSERT_EQUAL_PTR(blocks[1], new_head->next->next);

	TEST_ASSERT_EQUAL_PTR(   0xa000, new_head->next->next->addr);
	TEST_ASSERT_EQUAL(          512, new_head->next->next->size);
	TEST_ASSERT_EQUAL_PTR(blocks[2], new_head->next->next->next);

	TEST_ASSERT_EQUAL_PTR(  0x10000, new_head->next->next->next->addr);
	TEST_ASSERT_EQUAL(         1024, new_head->next->next->next->size);
	TEST_ASSERT_NULL(                new_head->next->next->next->next);
}

// Ajouter un block au milieu et merge avec le block suivant
void test_BlockAddToList_AddBlockInTheMiddleAndMergeWithNextOne(void)
{
	new_block->size = 256;
	new_block->addr = (void*)0xFF00;
	new_block->next = NULL;

	new_head = block_add_to_list(list, new_block, &pool);

	TEST_ASSERT_EQUAL_PTR(blocks[0], new_head);

	TEST_ASSERT_EQUAL_PTR(   0x4000, new_head->addr);
	TEST_ASSERT_EQUAL(           16, new_head->size);
	TEST_ASSERT_EQUAL_PTR(blocks[1], new_head->next);

	TEST_ASSERT_EQUAL_PTR(   0xa000, new_head->next->addr);
	TEST_ASSERT_EQUAL(          512, new_head->next->size);
	TEST_ASSERT_EQUAL_PTR(new_block, new_head->next->next);

	TEST_ASSERT_EQUAL_PTR(   0xFF00, new_head->next->next->addr);
	TEST_ASSERT_EQUAL(         1280, new_head->next->next->size);
	TEST_ASSERT_NULL(                new_head->next->next->next);
}

// Ajouter un block au milieu et merge avec le block precedent
void test_BlockAddToList_AddBlockInTheMiddleAndMergeWithPreviousOne(void)
{
	new_block->size = 512;
	new_block->addr = (void*)0xa200;
	new_block->next = NULL;

	new_head = block_add_to_list(list, new_block, &pool);

	TEST_ASSERT_EQUAL_PTR(blocks[0], new_head);

	TEST_ASSERT_EQUAL_PTR(   0x4000, new_head->addr);
	TEST_ASSERT_EQUAL(           16, new_head->size);
	TEST_ASSERT_EQUAL_PTR(blocks[1], new_head->next);

	TEST_ASSERT_EQUAL_PTR(   0xa000, new_head->next->addr);
	TEST_ASSERT_EQUAL(         1024, new_head->next->size);
	TEST_ASSERT_EQUAL_PTR(blocks[2], new_head->next->next);

	TEST_ASSERT_EQUAL_PTR(  0x10000, new_head->next->next->addr);
	TEST_ASSERT_EQUAL(         1024, new_head->next->next->size);
	TEST_ASSERT_NULL(                new_head->next->next->next);
}

// Ajouter un block au milieu et merge avec le block precedent et le block suivant
void test_BlockAddToList_AddBlockInTheMiddleAndMergeWithPreviousAndNextOne(void)
{
	new_block->size = 0x5e00;
	new_block->addr = (void*)0xa200;
	new_block->next = NULL;

	new_head = block_add_to_list(list, new_block, &pool);

	TEST_ASSERT_EQUAL_PTR(blocks[0], new_head);

	TEST_ASSERT_EQUAL_PTR(   0x4000, new_head->addr);
	TEST_ASSERT_EQUAL(           16, new_head->size);
	TEST_ASSERT_EQUAL_PTR(blocks[1], new_head->next);

	TEST_ASSERT_EQUAL_PTR(   0xa000, new_head->next->addr);
	TEST_ASSERT_EQUAL(       0x6400, new_head->next->size);
	TEST_ASSERT_NULL(                new_head->next->next);
}

// Ajouter un block a la fin
void test_BlockAddToList_AddBlockInTheEndWithNoMergeOfThePreviousOne(void)
{
	new_block->size = 0xff;
	new_block->addr = (void*)0xf0000;
	new_block->next = NULL;

	new_head = block_add_to_list(list, new_block, &pool);

	TEST_ASSERT_EQUAL_PTR(blocks[0], new_head);

	TEST_ASSERT_EQUAL_PTR(   0x4000, new_head->addr);
	TEST_ASSERT_EQUAL(         0x10, new_head->size);
	TEST_ASSERT_EQUAL_PTR(blocks[1], new_head->next);

	TEST_ASSERT_EQUAL_PTR(   0xa000, new_head->next->addr);
	TEST_ASSERT_EQUAL(        0x200, new_head->next->size);
	TEST_ASSERT_EQUAL_PTR(blocks[2], new_head->next->next);

	TEST_ASSERT_EQUAL_PTR(  0x10000, new_head->next->next->addr);
	TEST_ASSERT_EQUAL(        0x400, new_head->next->next->size);
	TEST_ASSERT_EQUAL_PTR(new_block, new_head->next->next->next);

	TEST_ASSERT_EQUAL_PTR(  0xf0000, new_head->next->next->next->addr);
	TEST_ASSERT_EQUAL(         0xff, new_head->next->next->next->size);
	TEST_ASSERT_NULL(                new_head->next->next->next->next);
}

// Ajouter un block a la fin et merge le precedent
void test_BlockAddToList_AddBlockInTheEndAndMergeWithThePreviousOne(void)
{
	new_block->size = 0xff;
	new_block->addr = (void*)0x10400;
	new_block->next = NULL;

	new_head = block_add_to_list(list, new_block, &pool);

	TEST_ASSERT_EQUAL_PTR(blocks[0], new_head);

	TEST_ASSERT_EQUAL_PTR(   0x4000, new_head->addr);
	TEST_ASSERT_EQUAL(         0x10, new_head->size);
	TEST_ASSERT_EQUAL_PTR(blocks[1], new_head->next);

	TEST_ASSERT_EQUAL_PTR(   0xa000, new_head->next->addr);
	TEST_ASSERT_EQUAL(        0x200, new_head->next->size);
	TEST_ASSERT_EQUAL_PTR(blocks[2], new_head->next->next);

	TEST_ASSERT_EQUAL_PTR(  0x10000, new_head->next->next->addr);
	TEST_ASSERT_EQUAL(        0x4ff, new_head->next->next->size);
	TEST_ASSERT_NULL(                new_head->next->next->next);
}
