#include "unity.h"
#include "ft_malloc.h"
#include <string.h>
#include <sys/mman.h>

#define MAX_BLOCKS_IN_POOL(psize)	(((psize) - sizeof(t_mem_pool)) / sizeof(t_mem_block))

static size_t		pool_size;
static t_mem_pool	*p;

void setUp(void)
{
}

void tearDown(void)
{
	p = NULL;
	pool_size = 0;
}

void test_PoolAllocNew_OnePage(void)
{
	pool_size = 4096;
	p = pool_alloc_new_pool(pool_size);

	TEST_ASSERT_EQUAL(MAX_BLOCKS_IN_POOL(pool_size), p->max);
}

void test_PoolAllocNew_TwoPage(void)
{
	pool_size = 4096 * 2;
	p = pool_alloc_new_pool(pool_size);

	TEST_ASSERT_EQUAL(MAX_BLOCKS_IN_POOL(pool_size), p->max);
}

void test_PoolAlloNew_WriteAllMemBlocksToSeeIfSanitizeSEGV(void)
{
	pool_size = 4096;
	p = pool_alloc_new_pool(pool_size);

	for (size_t i = 0; i < p->max; ++i)
	{
		p->blocks[i] = (t_mem_block){(void*)-1, (void*)-1, (size_t)-1};
	}

	size_t mem_blocks_size = MAX_BLOCKS_IN_POOL(pool_size);
	char expected[mem_blocks_size];
	memset(expected, 0xFF, mem_blocks_size);
	TEST_ASSERT_EQUAL_MEMORY(expected, &p->blocks[0], mem_blocks_size);
}

void test_PoolAlloNew_WriteAllMemBlocksToSeeIfSanitizeSEGV_2PagesThisTime(void)
{
	pool_size = 4096 * 2;
	p = pool_alloc_new_pool(pool_size);

	for (size_t i = 0; i < p->max; ++i)
	{
		p->blocks[i] = (t_mem_block){(void*)-1, (void*)-1, (size_t)-1};
	}

	size_t mem_blocks_size = MAX_BLOCKS_IN_POOL(pool_size);
	char expected[mem_blocks_size];
	memset(expected, 0xFF, mem_blocks_size);
	TEST_ASSERT_EQUAL_MEMORY(expected, &p->blocks[0], mem_blocks_size);
}

void test_PoolAlloNew_PagesizeOfZeroAsArgument(void)
{
	pool_size = 0;
	p = pool_alloc_new_pool(pool_size);

	TEST_ASSERT_NULL(p);
}

void test_PoolAlloNew_NotEnoughSpaceToHaveAtLeastOneBlock(void)
{
	pool_size = sizeof(t_mem_pool) + sizeof(t_mem_block) - 1;
	p = pool_alloc_new_pool(pool_size);

	TEST_ASSERT_NULL(p);
}

void test_PoolAlloNew_OnlyOneBlockInPool(void)
{
	pool_size = sizeof(t_mem_pool) + sizeof(t_mem_block);
	p = pool_alloc_new_pool(pool_size);

	TEST_ASSERT_NOT_NULL(p);
}

//void test_PoolAlloNew_WriteOneMoreMemBlocks_ItShouldFAIL(void)
//{
//	t_mem_pool *p = pool_alloc_new_pool(4096);
//
//	for (size_t i = 0; i < p->max+1; ++i)
//	{
//		p->blocks[i] = (t_mem_block){(void*)-1, (void*)-1, (size_t)-1};
//	}
//}
