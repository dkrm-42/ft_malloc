#include "unity.h"
#include "stdlib.h"
#include "ft_malloc.h"

static t_mem_block	*occupied;
static t_mem_block	*removed;
static void *addrs[] = {
	[0] = (void*)0x123,
	[1] = (void*)0x456,
	[2] = (void*)0x789
};


void setUp(void)
{
	t_mem_block **p = &occupied;
	for (size_t i = 0; i < 3; ++i)
	{
		*p = malloc(sizeof(t_mem_block));
		(*p)->addr = (void*)addrs[i];
		p = &((*p)->next);
	}
	*p = NULL;
	removed = (void*)0xfefe;
}

void tearDown(void)
{
	t_mem_block *tmp;

	while (occupied != NULL)
	{
		tmp = occupied->next;
		free(occupied);
		occupied = tmp;
	}
}

void test_OccupiedIsBlockPresent_OccupiedEqualsNULL(void)
{
	TEST_ASSERT_NULL(occupied_find_block_with_addr(NULL, (void*)0x1234));
	TEST_ASSERT_NULL(occupied_find_block_with_addr(NULL, NULL));
}

void test_OccupiedIsBlockPresent_AddrDoesntExistInOccupiedList(void)
{
	TEST_ASSERT_NULL(occupied_find_block_with_addr(occupied, (void*)0x12));
	TEST_ASSERT_NULL(occupied_find_block_with_addr(occupied, (void*)0x124));
	TEST_ASSERT_NULL(occupied_find_block_with_addr(occupied, NULL));
}

void test_OccupiedIsBlockPresent_AddrIsPresentInList(void)
{
	TEST_ASSERT_NOT_NULL(occupied_find_block_with_addr(occupied, (void*)0x123));
	TEST_ASSERT_NOT_NULL(occupied_find_block_with_addr(occupied, (void*)0x456));
	TEST_ASSERT_NOT_NULL(occupied_find_block_with_addr(occupied, (void*)0x789));
}

void test_OccupiedAddBlock_OccupiedEqualNULLShallAddBlockAsFirst(void)
{
	t_mem_block *b = malloc(sizeof(t_mem_block));
	b->addr = (void*)0x111;

	occupied = occupied_add_block_to_list(NULL, b);

	TEST_ASSERT_EQUAL_PTR(b->addr,  occupied->addr);
	TEST_ASSERT_NULL(				occupied->next);

	t_mem_block *c = malloc(sizeof(t_mem_block));
	c->addr = (void*)0x101;

	occupied = occupied_add_block_to_list(occupied, c);

	TEST_ASSERT_EQUAL_PTR(c->addr,  occupied->addr);
	TEST_ASSERT_EQUAL_PTR(b->addr,  occupied->next->addr);
	TEST_ASSERT_NULL(				occupied->next->next);
}

void test_OccupiedAddBlock_BlockShallBeFirst(void)
{
	t_mem_block *b = malloc(sizeof(t_mem_block));
	b->addr = (void*)0x111;

	occupied = occupied_add_block_to_list(occupied, b);

	TEST_ASSERT_EQUAL_PTR(b->addr,  occupied->addr);
	TEST_ASSERT_EQUAL_PTR(addrs[0], occupied->next->addr);
	TEST_ASSERT_EQUAL_PTR(addrs[1], occupied->next->next->addr);
	TEST_ASSERT_EQUAL_PTR(addrs[2], occupied->next->next->next->addr);
	TEST_ASSERT_NULL(				occupied->next->next->next->next);

	t_mem_block *c = malloc(sizeof(t_mem_block));
	c->addr = (void*)0x110;

	occupied = occupied_add_block_to_list(occupied, c);

	TEST_ASSERT_EQUAL_PTR(c->addr,  occupied->addr);
	TEST_ASSERT_EQUAL_PTR(b->addr,  occupied->next->addr);
	TEST_ASSERT_EQUAL_PTR(addrs[0], occupied->next->next->addr);
	TEST_ASSERT_EQUAL_PTR(addrs[1], occupied->next->next->next->addr);
	TEST_ASSERT_EQUAL_PTR(addrs[2], occupied->next->next->next->next->addr);
	TEST_ASSERT_NULL(				occupied->next->next->next->next->next);
}

void test_OccupiedAddBlock_BlockShallBeLast(void)
{
	t_mem_block *b = malloc(sizeof(t_mem_block));
	b->addr = (void*)0x1000;

	occupied = occupied_add_block_to_list(occupied, b);

	TEST_ASSERT_EQUAL_PTR(addrs[0], occupied->addr);
	TEST_ASSERT_EQUAL_PTR(addrs[1], occupied->next->addr);
	TEST_ASSERT_EQUAL_PTR(addrs[2], occupied->next->next->addr);
	TEST_ASSERT_EQUAL_PTR(b->addr,  occupied->next->next->next->addr);
	TEST_ASSERT_NULL(				occupied->next->next->next->next);

	t_mem_block *c = malloc(sizeof(t_mem_block));
	c->addr = (void*)0x1001;

	occupied = occupied_add_block_to_list(occupied, c);

	TEST_ASSERT_EQUAL_PTR(addrs[0], occupied->addr);
	TEST_ASSERT_EQUAL_PTR(addrs[1], occupied->next->addr);
	TEST_ASSERT_EQUAL_PTR(addrs[2], occupied->next->next->addr);
	TEST_ASSERT_EQUAL_PTR(b->addr,  occupied->next->next->next->addr);
	TEST_ASSERT_EQUAL_PTR(c->addr,  occupied->next->next->next->next->addr);
	TEST_ASSERT_NULL(				occupied->next->next->next->next->next);
}

void test_OccupiedAddBlock_BlockShallBeInTheMiddle(void)
{
	t_mem_block *b = malloc(sizeof(t_mem_block));
	b->addr = (void*)0x457;

	occupied = occupied_add_block_to_list(occupied, b);

	TEST_ASSERT_EQUAL_PTR(addrs[0], occupied->addr);
	TEST_ASSERT_EQUAL_PTR(addrs[1], occupied->next->addr);
	TEST_ASSERT_EQUAL_PTR(b->addr,  occupied->next->next->addr);
	TEST_ASSERT_EQUAL_PTR(addrs[2], occupied->next->next->next->addr);
	TEST_ASSERT_NULL(				occupied->next->next->next->next);
}

void test_OccupiedAddBlock_AddTwoSameBlocks(void)
{
	t_mem_block *b = malloc(sizeof(t_mem_block));
	b->addr = (void*)0x333;

	occupied = occupied_add_block_to_list(occupied, b);

	TEST_ASSERT_EQUAL_PTR(addrs[0], occupied->addr);
	TEST_ASSERT_EQUAL_PTR(b->addr,  occupied->next->addr);
	TEST_ASSERT_EQUAL_PTR(addrs[1], occupied->next->next->addr);
	TEST_ASSERT_EQUAL_PTR(addrs[2], occupied->next->next->next->addr);
	TEST_ASSERT_NULL(				occupied->next->next->next->next);

	t_mem_block *c = malloc(sizeof(t_mem_block));
	c->addr = (void*)0x333;

	occupied = occupied_add_block_to_list(occupied, c);

	TEST_ASSERT_EQUAL_PTR(addrs[0], occupied->addr);
	TEST_ASSERT_EQUAL_PTR(b->addr,  occupied->next->addr);
	TEST_ASSERT_EQUAL_PTR(c->addr,  occupied->next->next->addr);
	TEST_ASSERT_EQUAL_PTR(addrs[1], occupied->next->next->next->addr);
	TEST_ASSERT_EQUAL_PTR(addrs[2], occupied->next->next->next->next->addr);
	TEST_ASSERT_NULL(				occupied->next->next->next->next->next);
}

void test_OccupiedRemoveBlock_OccupiedEqualNULLShouldDoNothing(void)
{
	t_mem_block *b = malloc(sizeof(t_mem_block));
	b->addr = (void*)0x8888;

	occupied = occupied_remove_block_with_addr(NULL, b, &removed);

	TEST_ASSERT_NULL(occupied);
	TEST_ASSERT_NULL(removed);

	free(b);
}

void test_OccupiedRemoveBlock_RemoveLastBlock(void)
{
	occupied = occupied_remove_block_with_addr(occupied, addrs[2], &removed);

	TEST_ASSERT_EQUAL_PTR(addrs[2], removed->addr);
	TEST_ASSERT_EQUAL_PTR(addrs[0], occupied->addr);
	TEST_ASSERT_EQUAL_PTR(addrs[1], occupied->next->addr);
	TEST_ASSERT_NULL(				occupied->next->next);
}

void test_OccupiedRemoveBlock_RemoveFirstBlock(void)
{
	occupied = occupied_remove_block_with_addr(occupied, addrs[0], &removed);

	TEST_ASSERT_EQUAL_PTR(addrs[0], removed->addr);
	TEST_ASSERT_EQUAL_PTR(addrs[1], occupied->addr);
	TEST_ASSERT_EQUAL_PTR(addrs[2], occupied->next->addr);
	TEST_ASSERT_NULL(				occupied->next->next);
}

void test_OccupiedRemoveBlock_RemoveAllBlocksOccupiedShouldEndWithNull(void)
{
	occupied = occupied_remove_block_with_addr(occupied, addrs[0], &removed);

	TEST_ASSERT_EQUAL_PTR(addrs[0], removed->addr);
	TEST_ASSERT_EQUAL_PTR(addrs[1], occupied->addr);
	TEST_ASSERT_EQUAL_PTR(addrs[2], occupied->next->addr);
	TEST_ASSERT_NULL(				occupied->next->next);

	occupied = occupied_remove_block_with_addr(occupied, addrs[1], &removed);

	TEST_ASSERT_EQUAL_PTR(addrs[1], removed->addr);
	TEST_ASSERT_EQUAL_PTR(addrs[2], occupied->addr);
	TEST_ASSERT_NULL(				occupied->next);

	occupied = occupied_remove_block_with_addr(occupied, addrs[2], &removed);

	TEST_ASSERT_EQUAL_PTR(addrs[2], removed->addr);
	TEST_ASSERT_NULL(				occupied);
}

void test_OccupiedRemoveBlock_RemoveBlockThatDoesNotExist(void)
{
	occupied = occupied_remove_block_with_addr(occupied, (void*)0xffff, &removed);

	TEST_ASSERT_NULL(				removed);
	TEST_ASSERT_EQUAL_PTR(addrs[0], occupied->addr);
	TEST_ASSERT_EQUAL_PTR(addrs[1], occupied->next->addr);
	TEST_ASSERT_EQUAL_PTR(addrs[2], occupied->next->next->addr);
	TEST_ASSERT_NULL(				occupied->next->next->next);
}

void test_OccupiedRemoveBlock_RemoveNULLBlock(void)
{
	occupied = occupied_remove_block_with_addr(occupied, NULL, &removed);

	TEST_ASSERT_NULL(				removed);
	TEST_ASSERT_EQUAL_PTR(addrs[0], occupied->addr);
	TEST_ASSERT_EQUAL_PTR(addrs[1], occupied->next->addr);
	TEST_ASSERT_EQUAL_PTR(addrs[2], occupied->next->next->addr);
	TEST_ASSERT_NULL(				occupied->next->next->next);
}
