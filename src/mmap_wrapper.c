/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mmap_wrapper.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: djean <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/25 12:29:49 by djean             #+#    #+#             */
/*   Updated: 2018/11/25 12:29:56 by djean            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/mman.h>
#include <stdlib.h>

#define MMAP_PROT		(PROT_READ|PROT_WRITE)
#define MMAP_FLAGS		(MAP_ANONYMOUS|MAP_PRIVATE)

void	*internal_mmap_wrapper(size_t size)
{
	void	*p;

	p = mmap(NULL, size, MMAP_PROT, MMAP_FLAGS, -1, 0);
	if (p == MAP_FAILED)
		return (NULL);
	return (p);
}

void	internal_munmap_wrapper(void *p, size_t size)
{
	munmap(p, size);
}
