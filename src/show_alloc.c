/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   show_alloc.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: djean <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/25 12:29:49 by djean             #+#    #+#             */
/*   Updated: 2018/11/25 17:25:37 by djean            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "malloc.h"

extern t_malloc	g_internal_malloc;

/*
** Taille max d'une ligne
** "0xFFFFFFFFFFFFFFFF - 0xFFFFFFFFFFFFFFFF : 18446744073709551615 octets\n"
*/

static inline size_t	build_block_line(const t_mem_block *block, char *line)
{
	char	buf[32];

	buf[0] = '\0';
	ft_strcat(line, "0x");
	ft_utoa_base_r(buf, (uintptr_t)block->addr, 16);
	ft_strcat(line, buf);
	ft_strcat(line, " - ");
	ft_strcat(line, "0x");
	ft_utoa_base_r(buf, (uintptr_t)((char*)block->addr + block->size), 16);
	ft_strcat(line, buf);
	ft_strcat(line, " : ");
	ft_utoa_base_r(buf, block->size, 10);
	ft_strcat(line, buf);
	ft_strcat(line, " octets\n");
	return (ft_strlen(line));
}

static inline size_t	print_arena(const t_mem_block *occupied,
		size_t arena_min_size,
		size_t arena_max_size)
{
	const t_mem_block	*block;
	char				line[128];
	size_t				len;
	size_t				bytes;

	block = occupied;
	bytes = 0;
	while (block)
	{
		line[0] = '\0';
		if (block->size > arena_min_size && block->size <= arena_max_size)
		{
			len = build_block_line(block, line);
			write(1, line, len);
			bytes += block->size;
		}
		block = block->next;
	}
	return (bytes);
}

static inline void		print_total(size_t bytes)
{
	char	total[64];
	char	size[32];

	total[0] = '\0';
	size[0] = '\0';
	ft_utoa_base_r(size, bytes, 10);
	ft_strcat(total, "Total : ");
	ft_strcat(total, size);
	ft_strcat(total, " octets\n");
	write(1, total, ft_strlen(total));
}

void					show_alloc_mem(void)
{
	size_t	bytes;

	bytes = 0;
	write(1, "TINY\n", 5);
	bytes += print_arena(g_internal_malloc.occupied,
			0,
			ARENA_TINY_MAX_ALLOC_SIZE);
	write(1, "SMALL\n", 6);
	bytes += print_arena(g_internal_malloc.occupied,
			ARENA_TINY_MAX_ALLOC_SIZE,
			ARENA_SMALL_MAX_ALLOC_SIZE);
	write(1, "LARGE\n", 6);
	bytes += print_arena(g_internal_malloc.occupied,
			ARENA_SMALL_MAX_ALLOC_SIZE,
			SIZE_MAX);
	print_total(bytes);
}
