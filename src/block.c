/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   block.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: djean <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/25 12:29:49 by djean             #+#    #+#             */
/*   Updated: 2018/11/25 12:44:13 by djean            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

/*
** Merge 'block' with the next blocks if they are memory-adjacent
*/

static inline t_mem_block	*block_merge_next_block(t_mem_pool **pool,
		t_mem_block *block)
{
	t_mem_block	*next_block;
	void		*current_block_end_addr;

	next_block = block->next;
	current_block_end_addr = (char*)block->addr + block->size;
	if (next_block != NULL && current_block_end_addr == next_block->addr)
	{
		block->size += next_block->size;
		block->next = next_block->next;
		*pool = pool_release_block(*pool, next_block);
	}
	return (block);
}

t_mem_block					*block_add_to_list(t_mem_block *list,
		t_mem_block *new_block,
		t_mem_pool **pool)
{
	t_mem_block	**ptr_to_block;
	t_mem_block	*current_block;
	t_mem_block	*prev_block;

	ptr_to_block = &list;
	current_block = NULL;
	prev_block = NULL;
	while ((current_block = *ptr_to_block) != NULL)
	{
		if (new_block->addr <= current_block->addr)
			break ;
		prev_block = current_block;
		ptr_to_block = &(current_block->next);
	}
	new_block->next = current_block;
	*ptr_to_block = new_block;
	block_merge_next_block(pool, new_block);
	if (prev_block != NULL)
		block_merge_next_block(pool, prev_block);
	return (list);
}

void						block_split_to_size(t_mem_block *block_to_split,
		t_mem_block *new_block,
		size_t requested_size)
{
	new_block->addr = block_to_split->addr;
	new_block->next = NULL;
	new_block->size = requested_size;
	block_to_split->addr = (char*)block_to_split->addr + requested_size;
	block_to_split->size -= requested_size;
}

t_mem_block					*block_find_fittest_for_size(t_mem_block **list,
		t_mem_pool **pool,
		size_t requested_size)
{
	t_mem_block		**ptr_to_block;
	t_mem_block		*best_block;
	t_mem_block		*new_block;

	ptr_to_block = list;
	best_block = *ptr_to_block;
	while (*ptr_to_block != NULL)
	{
		if ((*ptr_to_block)->size == requested_size)
		{
			best_block = *ptr_to_block;
			*ptr_to_block = best_block->next;
			best_block->next = NULL;
			return (best_block);
		}
		if ((*ptr_to_block)->size > requested_size
				&& (*ptr_to_block)->size < best_block->size)
			best_block = *ptr_to_block;
		ptr_to_block = &((*ptr_to_block)->next);
	}
	if (best_block == NULL || best_block->size < requested_size)
		return (NULL);
	if ((new_block = pool_get_free_block(pool)) != NULL)
		block_split_to_size(best_block, new_block, requested_size);
	return (new_block);
}
