/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: djean <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/25 12:29:49 by djean             #+#    #+#             */
/*   Updated: 2018/11/25 12:50:40 by djean            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

extern t_malloc g_internal_malloc;

static inline void	internal_munmap_unnecessary_region(t_arena *arena)
{
	t_mem_block	*block;

	block = arena->freelist;
	while (block != NULL)
	{
		if (block->size > arena->region_size
				&& block->size % arena->region_size == 0)
		{
			internal_munmap_wrapper((char*)block->addr + arena->region_size,
					block->size - arena->region_size);
			block->size -= arena->region_size;
			break ;
		}
		block = block->next;
	}
}

void				free(void *ptr)
{
	t_arena		*arena;
	t_mem_block	*block;
	t_mem_block	*occupied;
	t_mem_pool	**pool;

	occupied = g_internal_malloc.occupied;
	pool = &g_internal_malloc.pool;
	if (ptr == NULL || occupied_find_block_with_addr(occupied, ptr) == NULL)
		return ;
	occupied = occupied_remove_block_with_addr(occupied, ptr, &block);
	arena = arena_get_arena_associated_with_size(&g_internal_malloc,
			block->size);
	if (arena != NULL)
	{
		arena->freelist = block_add_to_list(arena->freelist, block, pool);
		internal_munmap_unnecessary_region(arena);
	}
	else
		internal_munmap_wrapper(ptr, block->size);
	g_internal_malloc.occupied = occupied;
}
