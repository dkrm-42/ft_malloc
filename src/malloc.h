/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: djean <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/25 13:43:54 by djean             #+#    #+#             */
/*   Updated: 2018/11/25 13:50:03 by djean            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_MALLOC_H
# define FT_MALLOC_H

# include <stdlib.h>
# include <stdbool.h>

# define ARENA_TINY_REGION_SIZE		(2097152)
# define ARENA_TINY_MAX_ALLOC_SIZE	(1024)
# define ARENA_SMALL_REGION_SIZE	(16777216)
# define ARENA_SMALL_MAX_ALLOC_SIZE	(16384)
# define POOL_SIZE					(2*4096)
# define ROUND_UP_16(n)			((n) % 16U ? ((n) + 16U) & (~0U - 15U) : (n))

typedef struct	s_mem_block
{
	void				*addr;
	struct s_mem_block	*next;
	size_t				size;
}				t_mem_block;

/*
** - freelist :: Liste des blocks libre pour allocation
** - region_size :: Taille du mapping où stocker les allocations
*/
typedef struct	s_arena
{
	t_mem_block	*freelist;
	size_t		region_size;
}				t_arena;

typedef struct	s_mem_pool
{
	size_t				allocated_size;
	size_t				max;
	size_t				used;
	size_t				offset;
	struct s_mem_pool	*next;
	struct s_mem_block	*freelist;
	struct s_mem_block	blocks[];
}				t_mem_pool;

typedef struct	s_malloc
{
	t_arena			tiny;
	t_arena			small;
	t_mem_block		*occupied;
	t_mem_pool		*pool;
	size_t			pagesize;
}				t_malloc;

/*
** Arena
*/

/*
** Return a block containing a new memory mapping for the given arena size
*/
t_mem_block		*arena_alloc_new_region(t_arena *arena, t_mem_block *region);

/*
** Find an unused memory block of size 'block_size' in the given arena
** Return NULL if there is none.
*/
t_mem_block		*arena_find_free_block(t_arena *arena, t_mem_pool **pool,
		size_t block_size);

/*
** Return 'true' if the arena's freelist is empty
*/
bool			arena_is_freelist_empty(t_arena *arena);

/*
** Return the corresponding arena for a given 'block'
*/
t_arena			*arena_get_arena_associated_with_size(const t_malloc *m,
		size_t size);

/*
** Occupied
*/

/*
** Add 'block' to the list of 'occupied' blocks
** Return the head of the new 'occupied' list
*/
t_mem_block		*occupied_add_block_to_list(t_mem_block *occupied,
		t_mem_block *block);

/*
** Return a pointer to the block containing the address 'addr',
** in the 'occupied' list. NULL if not
*/
t_mem_block		*occupied_find_block_with_addr(t_mem_block *occupied,
		void *addr);

/*
** Remove the block corresponding to the 'addr' address from the 'occupied' list
** The removed block is assigned to 'removed',
** or NULL if there is no matching block.
** Return the new head of the 'occupied' list
*/
t_mem_block		*occupied_remove_block_with_addr(t_mem_block *occupied,
		void *addr,
		t_mem_block **removed);

/*
** Internal
*/

/*
** Implementations from libft
*/
void			*ft_memcpy(void *dst, void *src, size_t size);
size_t			ft_strlen(const char *s);
char			*ft_strcat(char *s1, const char *s2);
char			*ft_strrev(char *str);
char			*ft_utoa_base_r(char *buf, unsigned long long n, unsigned base);

/*
** Wrapper arround `mmap` and `munmap` functions
*/
void			*internal_mmap_wrapper(size_t size);
void			*internal_munmap_wrapper(void *p, size_t size);

/*
** Show memory allocations
*/
void			show_alloc_mem(void);

/*
** Block
*/

/*
** Add 'new_block' to 'list', sorted by addr
** Adjacent blocks are merged together
** Return the head of the new 'list'
*/
t_mem_block		*block_add_to_list(t_mem_block *list,
		t_mem_block *new_block,
		t_mem_pool **pool);

/*
** Split 'block_to_split' to the requested size
** The block with the requested size is returned
** The 'block_to_split' is reduced by 'requested_size'
*/
void			block_split_to_size(t_mem_block *block_to_split,
		t_mem_block *new_block,
		size_t requested_size);

/*
** Find fittest block, according to 'size', in the 'list' and returns it
** The remaining space is inserted in the 'list'
** The head of the 'list' is update if necessary
*/
t_mem_block		*block_find_fittest_for_size(t_mem_block **list,
		t_mem_pool **pool,
		size_t size);

/*
** Pool
*/

/*
** Return a new pool ready to use
*/
t_mem_pool		*pool_alloc_new_pool(size_t pool_size);

/*
** Return a free block from the 'pool'
** Either the head of the pool's freelist
** or the next unused block in the 'blocks' array.
** If everything is used, allocate a new pool
*/
t_mem_block		*pool_get_free_block(t_mem_pool **pool);

/*
** Add `block' to the head of the pool's freelist
** If the pool is empty and is not the only one, then `unmap` it
*/
t_mem_pool		*pool_release_block(t_mem_pool *pool, t_mem_block *block);

#endif
