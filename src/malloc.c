/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: djean <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/25 12:29:49 by djean             #+#    #+#             */
/*   Updated: 2018/11/25 13:02:13 by djean            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <unistd.h>
#include "malloc.h"

extern t_malloc	g_internal_malloc;
t_malloc		g_internal_malloc;

static inline void	internal_malloc_init(void)
{
	if (g_internal_malloc.pagesize == 0)
	{
		g_internal_malloc.pagesize = getpagesize();
		g_internal_malloc.occupied = NULL;
		g_internal_malloc.pool = NULL;
		g_internal_malloc.tiny.region_size = ARENA_TINY_REGION_SIZE;
		g_internal_malloc.tiny.freelist = NULL;
		g_internal_malloc.small.region_size = ARENA_SMALL_REGION_SIZE;
		g_internal_malloc.small.freelist = NULL;
	}
}

void				*malloc(size_t size)
{
	t_arena		*arena;
	t_mem_block	*block;
	t_mem_block	*occupied;
	t_mem_pool	**pool;

	internal_malloc_init();
	if (size == 0)
		return (NULL);
	size = ROUND_UP_16(size);
	occupied = g_internal_malloc.occupied;
	pool = &g_internal_malloc.pool;
	arena = arena_get_arena_associated_with_size(&g_internal_malloc, size);
	if (arena != NULL)
		block = arena_find_free_block(arena, pool, size);
	else
	{
		block = pool_get_free_block(pool);
		if (block == NULL)
			return (NULL);
		block->addr = internal_mmap_wrapper(size);
		block->size = size;
	}
	g_internal_malloc.occupied = occupied_add_block_to_list(occupied, block);
	return (block->addr);
}
