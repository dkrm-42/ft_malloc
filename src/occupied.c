/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   occupied.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: djean <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/25 12:29:49 by djean             #+#    #+#             */
/*   Updated: 2018/11/25 13:54:23 by djean            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdbool.h>
#include "malloc.h"

t_mem_block	*occupied_find_block_with_addr(t_mem_block *occupied, void *addr)
{
	t_mem_block	*block;

	block = occupied;
	while (block != NULL)
	{
		if (block->addr == addr)
			return (block);
		block = block->next;
	}
	return (NULL);
}

t_mem_block	*occupied_add_block_to_list(t_mem_block *occupied,
		t_mem_block *block)
{
	t_mem_block	**ptr_to_block;

	ptr_to_block = &occupied;
	while (*ptr_to_block)
	{
		if (block->addr < (*ptr_to_block)->addr)
			break ;
		ptr_to_block = &((*ptr_to_block)->next);
	}
	block->next = *ptr_to_block;
	*ptr_to_block = block;
	return (occupied);
}

t_mem_block	*occupied_remove_block_with_addr(t_mem_block *occupied, void *addr,
		t_mem_block **removed)
{
	t_mem_block	**ptr_to_block;

	*removed = NULL;
	ptr_to_block = &occupied;
	while (*ptr_to_block)
	{
		if (addr == (*ptr_to_block)->addr)
		{
			*removed = *ptr_to_block;
			*ptr_to_block = (*removed)->next;
			break ;
		}
		ptr_to_block = &((*ptr_to_block)->next);
	}
	return (occupied);
}
