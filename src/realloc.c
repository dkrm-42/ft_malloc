/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   realloc.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: djean <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/25 12:29:49 by djean             #+#    #+#             */
/*   Updated: 2018/11/25 13:59:19 by djean            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

extern t_malloc	g_internal_malloc;

static inline void	*internal_memory_reallocation(void *ptr, size_t new_size)
{
	void		*new_ptr;
	t_mem_block *occupied_block;
	size_t		old_size;

	occupied_block = occupied_find_block_with_addr(g_internal_malloc.occupied,
			ptr);
	if (occupied_block == NULL)
		return (NULL);
	old_size = occupied_block->size;
	if (new_size == old_size)
		return (ptr);
	new_ptr = malloc(new_size);
	ft_memcpy(new_ptr, ptr, (new_size > old_size) ? old_size : new_size);
	free(occupied_block->addr);
	return (new_ptr);
}

void				*realloc(void *ptr, size_t size)
{
	void		*new_ptr;

	new_ptr = ptr;
	if (ptr == NULL)
		new_ptr = malloc(size);
	else if (size == 0)
		free(ptr);
	else
		new_ptr = internal_memory_reallocation(ptr, ROUND_UP_16(size));
	return (new_ptr);
}
