/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   arena.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: djean <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/25 12:29:49 by djean             #+#    #+#             */
/*   Updated: 2018/11/25 12:31:16 by djean            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdbool.h>
#include "malloc.h"

bool		arena_is_freelist_empty(t_arena *arena)
{
	return (arena->freelist == NULL);
}

t_arena		*arena_get_arena_associated_with_size(const t_malloc *m,
		size_t size)
{
	if (size <= ARENA_TINY_MAX_ALLOC_SIZE)
		return ((t_arena*)&m->tiny);
	else if (size <= ARENA_SMALL_MAX_ALLOC_SIZE)
		return ((t_arena*)&m->small);
	return (NULL);
}

t_mem_block	*arena_alloc_new_region(t_arena *arena, t_mem_block *region)
{
	void		*mapping;

	mapping = internal_mmap_wrapper(arena->region_size);
	if (mapping == NULL)
		return (NULL);
	region->addr = mapping;
	region->size = arena->region_size;
	return (region);
}

t_mem_block	*arena_find_free_block(t_arena *arena, t_mem_pool **pool,
		size_t requested_size)
{
	t_mem_block	*free_block;
	t_mem_block	*region;

	free_block = block_find_fittest_for_size(&arena->freelist, pool,
			requested_size);
	if (free_block == NULL)
	{
		free_block = pool_get_free_block(pool);
		region = pool_get_free_block(pool);
		if (free_block == NULL || region == NULL)
			return (NULL);
		arena_alloc_new_region(arena, region);
		block_split_to_size(region, free_block, requested_size);
		arena->freelist = block_add_to_list(arena->freelist, region, pool);
	}
	return (free_block);
}
