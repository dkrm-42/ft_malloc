/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pool.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: djean <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/25 12:29:49 by djean             #+#    #+#             */
/*   Updated: 2018/11/25 13:58:42 by djean            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "malloc.h"

t_mem_pool					*pool_alloc_new_pool(size_t pool_size)
{
	t_mem_pool	*mapping;

	if (pool_size < (sizeof(t_mem_pool) + sizeof(t_mem_block)))
		return (NULL);
	mapping = (t_mem_pool*)internal_mmap_wrapper(pool_size);
	if (mapping != NULL)
	{
		mapping->allocated_size = pool_size;
		mapping->max = (pool_size - sizeof(t_mem_pool)) / sizeof(t_mem_block);
		mapping->used = 0;
		mapping->offset = 0;
		mapping->next = NULL;
		mapping->freelist = NULL;
	}
	return (mapping);
}

static inline t_mem_block	*get_first_free_block_from_pool(t_mem_pool *pool)
{
	t_mem_block	*block;

	if (pool->freelist != NULL)
	{
		block = pool->freelist;
		pool->freelist = pool->freelist->next;
		pool->used += 1;
	}
	else if (pool->offset < pool->max)
	{
		block = &pool->blocks[pool->offset];
		pool->offset += 1;
		pool->used += 1;
	}
	else
	{
		block = NULL;
	}
	return (block);
}

t_mem_block					*pool_get_free_block(t_mem_pool **pool)
{
	t_mem_block	*block;
	t_mem_pool	**ptr_to_pool;
	t_mem_pool	*new_pool;

	ptr_to_pool = pool;
	while (*ptr_to_pool != NULL)
	{
		block = get_first_free_block_from_pool(*ptr_to_pool);
		if (block != NULL)
			return (block);
		ptr_to_pool = &((*ptr_to_pool)->next);
	}
	new_pool = pool_alloc_new_pool(POOL_SIZE);
	if (new_pool == NULL)
		return (NULL);
	*ptr_to_pool = new_pool;
	return (get_first_free_block_from_pool(new_pool));
}

t_mem_pool					*pool_release_block(t_mem_pool *pool,
		t_mem_block *block)
{
	t_mem_pool	**ptr_to_pool;
	t_mem_pool	*node;

	ptr_to_pool = &pool;
	while (*ptr_to_pool != NULL)
	{
		node = *ptr_to_pool;
		if ((void*)block > (void*)node
			&& (void*)block < (void*)((char*)node + node->allocated_size))
		{
			block->next = node->freelist;
			node->freelist = block;
			node->used -= 1;
			if (node->offset == node->max && node->used == 0)
			{
				*ptr_to_pool = node->next;
				internal_munmap_wrapper(node, node->allocated_size);
			}
			break ;
		}
		ptr_to_pool = &((*ptr_to_pool)->next);
	}
	return (pool);
}
