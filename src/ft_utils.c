/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_utils.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: djean <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/25 12:29:49 by djean             #+#    #+#             */
/*   Updated: 2018/11/25 13:00:09 by djean            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

size_t	ft_strlen(const char *s)
{
	size_t	len;

	len = 0;
	while (s[len])
		len += 1;
	return (len);
}

char	*ft_strcat(char *s1, const char *s2)
{
	ft_memcpy(s1 + ft_strlen(s1), (void*)s2, ft_strlen(s2) + 1);
	return (s1);
}

char	*ft_strrev(char *str)
{
	size_t	half;
	size_t	len;
	size_t	i;
	char	tmp;

	len = ft_strlen(str);
	i = 0;
	if (len % 2 == 0)
		half = len / 2;
	else
		half = (len - 1) / 2;
	--len;
	while (i < half)
	{
		tmp = str[i];
		str[i] = str[len - i];
		str[len - i++] = tmp;
	}
	return (str);
}

char	*ft_utoa_base_r(char *buf, unsigned long long n, unsigned base)
{
	char	*charset;
	size_t	i;

	if (base > 16)
		return (NULL);
	charset = "0123456789abcdef";
	i = 0;
	if (n == 0)
		buf[i++] = '0';
	while (n != 0)
	{
		buf[i++] = charset[n % base];
		n /= base;
	}
	buf[i] = '\0';
	return (ft_strrev(buf));
}
