include extras/flags.mk

ifeq ($(HOSTTYPE),)
	HOSTTYPE := $(shell uname -m)_$(shell uname -s)
endif

NAME = libft_malloc_$(HOSTTYPE).so


# Sources
SRC_PATH	+= src
SOURCES		+= malloc.c
SOURCES		+= realloc.c
SOURCES		+= free.c
SOURCES		+= mmap_wrapper.c
SOURCES		+= pool.c
SOURCES		+= occupied.c
SOURCES		+= block.c
SOURCES		+= arena.c
SOURCES		+= show_alloc.c
SOURCES		+= ft_memcpy.c
SOURCES		+= ft_utils.c


# Generation
BUILD_PATH	= build
INC_PATH	+= src
CFLAGS		+= $(addprefix -I,$(INC_PATH))
vpath %.c $(SRC_PATH) $(addprefix $(SRC_PATH)/,$(SRC_SUBDIR))


# Object files
OBJ_PATH	= $(BUILD_PATH)/obj
OBJECTS		= $(SOURCES:%.c=$(OBJ_PATH)/%.o)
BUILD_DIR	= $(OBJ_PATH)


# Core rules
.PHONY: all clean fclean re check

.SECONDARY: $(OBJECTS)

all: $(NAME)

$(NAME): $(OBJECTS)
	$(CC) -shared $(LDFLAGS) -o $@ $^
	ln -fs $@ libft_malloc.so

archive: $(OBJECTS)
	cd $(INC_PATH) && ln -sf malloc.h ft_malloc.h
	ar rcs libft_malloc.a $^

$(OBJECTS): $(OBJ_PATH)/%.o: %.c | $(OBJ_PATH)
	$(CC) $(CFLAGS) -o $@ -c $<

$(BUILD_DIR):
	@-mkdir -p $@

clean:
	$(RM) -r $(OBJ_PATH)

fclean: clean
	$(RM) $(NAME)
	$(RM) libft_malloc.so
	$(RM) libft_malloc.a

re: fclean all

check: archive
	cd test && ./RunTests.sh

scale: all
	cd test/scale && ./runAll.fish

include extras/utils.mk
