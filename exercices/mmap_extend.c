#include <unistd.h>
#include <stdlib.h>
#include <strings.h>
#include <stdio.h>
#include <sys/mman.h>
#include <assert.h>

int main(int argc, char *argv[])
{
  (void)argc;
  (void)argv;

  int pgsize = getpagesize();
  printf("Page size: %d\n", pgsize);

  char *p = mmap(NULL, pgsize, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
  if (p == MAP_FAILED)
    {
      perror("Error: ");
      exit(1);
    }
  printf("First map: %p\n", p);

  printf("Write 'hello world' to offset 0x123\n");
  char *s = "hello world";
  char *pp = p + 0x123;
  for (size_t i = 0; s[i]; i++)
    {
      pp[i] = s[i];
      printf("--> write '%c' to '%p'\n", s[i], pp + i);
    }
  printf("Display p+0x123: %s\n", pp);
  assert(!strcmp(pp, s));


  printf("Try to increase mapping size\n");
  char *q = mmap(p + pgsize, pgsize, PROT_READ|PROT_WRITE, MAP_ANONYMOUS|MAP_PRIVATE, -1, 0);
  if (q == MAP_FAILED)
    {
      perror("Error: ");
      exit(1);
    }
  printf("Second map: %p\n", q);
  assert(p + pgsize == q);

  printf("Write 'hello world' on 2 pages\n");
  char *qq = q - 5;
  for (size_t i = 0; s[i]; i++)
    {
      qq[i] = s[i];
      printf("--> write '%c' to '%p'\n", s[i], qq + i);
    }

  printf("Display string on 2 pages: %s\n", qq);
  assert(!strcmp(pp, s));
  printf("Display p+0x123: %s\n", pp);
  assert(!strcmp(qq, s));


  printf("Try to increase mapping size, realloc all mapping\n");
  char *r = mmap(p, pgsize * 3, PROT_READ|PROT_WRITE, MAP_ANONYMOUS|MAP_PRIVATE, -1, 0);
  if (r == MAP_FAILED)
    {
      perror("Error: ");
      exit(1);
    }
  printf("Third map: %p\n", r);
  printf("Display string on 2 pages: %s\n", qq);
  printf("Display p+0x123: %s\n", pp);
}
